import Vue from 'vue'
import App from './App.vue'
import router from './router'
import './scss/index.scss'
import { getList } from './utils'

import axios from 'axios'
import MockAdapter from 'axios-mock-adapter'

const mock = new MockAdapter(axios)


mock.onGet('/api/deals/').reply((config) => {
  const list = getList(config.limit, config.page)
  return [200, {
    row: list
  }]
})


Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
