const api = [
  {
    dealTitle: 'FREE Drinks & Wi-Fi! No Fly Paris & Spain',
    cruiseCompany: 'Royal Caribbean',
    shipName: 'Explorer of the Seas',
    date: '5 October 2019',
    nights: '6',
    itinerary: [
      'Southampton',
      'Vigo',
      'Gijon',
      'Paris (Le Havre)',
      'Southampton'
    ],
    price: '699',
    detailsLinkName: 'View More Royal Caribbean Cruises from Southampton',
    detailsLink: 'https://www.cruise.co.uk/holidays/royal-caribbean-southampton-sailings-2019/?linkid=top10deals',
    descriptionPoints: [
      'FREE Drinks worth $780 per Cabin - Book By 5 Nov 18',
      'Book with www.CRUISE.co.uk for £1pp Deposit & $40 per cabin Exclusive Onboard Spend'
    ],
    quoteLink: 'https://www.cruise.co.uk/checkAvailability.php?offerId=145285&DOLIVE=YES&linkid=top10deals/'
  },
  {
    dealTitle: 'New Price Drop! The Beaches Of Bali With The Best of Thailand & Vietnam',
    cruiseCompany: 'Princess Cruises',
    shipName: 'Sapphire Princess',
    date: '20 Feb 2020',
    nights: '16',
    itinerary: [
      'Fly UK to Singapore - 2 Nights Hotel Stay',
      'Ko Samui(Thailand)',
      'Bangkok(Laem Chabang)',
      'Ho Chi Minh City(Vietnam)',
      'Singapore',
      'Fly to Bali - 5 Nights Hotel Stay',
      'Fly UK'
    ],
    price: '1649',
    descriptionPoints: [
      '5 Nights 4 * B & B Hotel Stay in Bali',
      '2 Nights 5 * Central Hotel Stay in Singapore'
    ],
    detailsLinkName: 'Click Here For Detailed Itinarary',
    detailsLink: 'https://www.cruise.co.uk/tailor-made-cruises/princess-cruises/sapphire-princess/20-feb-20/18423/?linkid=top10deals',
    quoteLink: 'https://www.cruise.co.uk/quote-147120/?linkid=top10deals'
  },
  {
    dealTitle: 'BEST SELLER! No-Fly Canary Islands',
    cruiseCompany: 'Princess Cruises',
    shipName: 'Sapphire Princess',
    date: '16 Apr 2019',
    nights: '11',
    itinerary: [
      'Southampton',
      'Vigo',
      'Madeira',
      'Tenerife',
      'Las Palmas',
      'Lanzarote',
      'Southampton'
    ],
    price: '1149',
    descriptionPoints: [
      'Up to $400 Per Cabin Onboard Spend From Princess Cruises',
      'Book With www.CRUISE.co.uk For £1pp Deposit * & $60 FREE Bonus Onboard Spend Per Cabin'
    ],
    detailsLinkName: 'View All 2019 Princess Cruises from Southampton in One Place',
    detailsLink: 'https://www.cruise.co.uk/holidays/princess-2019-cruises-from-southampton/?linkid=top10deals/',
    quoteLink: 'https://www.cruise.co.uk/checkAvailability.php?offerId=133781&DOLIVE=YES/?linkid=top10deals/'
  },
  {
    dealTitle: 'BEST SELLER! 5* Baltics & Scandinavia with Overnight St. Petersburg',
    cruiseCompany: 'Cunard ',
    shipName: 'Queen Elizabeth',
    date: '11 August 2019',
    nights: '14',
    itinerary: [
      'Southampton',
      'Skagen(Denmark)',
      'Copenhagen',
      'St.Petersburg(Overnight)',
      'Helsinki',
      'Stockholm',
      'Warnemunde(Germany)',
      'Southampton'
    ],
    price: '1299',
    descriptionPoints: [
      'Spend 2 Days Onboard in Iconic St.Petersburg',
      'Book with www.CRUISE.co.uk for £1pp Deposit & $70 per cabin Exclusive Onboard Spend'
    ],
    detailsLinkName: 'View All Cunard 2019 Deals from Southampton',
    detailsLink: 'https://www.cruise.co.uk/holidays/cunard-2019-cruises-from-southampton/?linkid=top10deals',
    quoteLink: 'https://www.cruise.co.uk/checkAvailability.php?offerId=129547&DOLIVE=YES/&noint=1&linkid=top10deals/'
  },
  {
    dealTitle: 'NEW EXCLUSIVE! Sri Lanka, Dubai, Goa & Thailand with All Inclusive Drinks',
    cruiseCompany: 'Marella Cruises',
    shipName: 'Marella Discovery',
    date: '06 Dec 2018',
    nights: '14',
    itinerary: [
      'Dubai(Port Rashid - Overnight)',
      'India(Mumbai)',
      'Goa',
      'Mangalore',
      'Kerala(Cochin - Overnight)',
      'Sri Lanka(Colombo)',
      'Indonesia(Sabang)',
      'Thailand(Phuket)',
      'Malaysia(Langkawi)'
    ],
    price: '1129',
    descriptionPoints: [
      'FREE All Inclusive Drinks, Entertainment, Tips & Service Charges ',
      'Direct Return Flights from Gatwick - Birmingham Available *'
    ],
    detailsLinkName: 'View All 2019 Marella Cruises with Drinks',
    detailsLink: 'https://www.cruise.co.uk/holidays/marella-2019-cruises/',
    quoteLink: 'https://www.cruise.co.uk/checkAvailability.php?offerId=130921&DOLIVE=YES/'
  },
  {
    dealTitle: 'EXCLUSIVE - Capetown To Venice With Seychelles & Mauritius Overnights',
    cruiseCompany: 'MSC Cruises',
    shipName: 'Orchestra ',
    date: '10 April 2020',
    nights: '33',
    itinerary: [
      '3 Night Stay in Central Cape Town',
      'Port Elizabeth',
      'Durban',
      'Reunion',
      'Mauritius(Port Louis - Overnight)',
      'Seychelles(Port Victoria - Overnight)',
      'Jordan(Aqaba)',
      'Suez Canal Transit',
      'Katakolon(Greece)',
      'Split'
    ],
    price: '2399',
    descriptionPoints: [
      '2 Tours Included: Table Mountain With City Tour & Winelands Experience',
      '3 Nights 4 * Central Capetown Hotel Stay',
      '29 Nights Full Board Crusing Onboard MSC Orchestra'
    ],
    detailsLinkName: 'Click Here To View The Full Details',
    detailsLink: 'https://www.cruise.co.uk/tailor-made-cruises/msc-cruises/msc-orchestra/10-apr-20/18387/',
    quoteLink: 'https://www.cruise.co.uk/tailor-made-cruises/msc-cruises/msc-orchestra/10-apr-20/18387/'
  },
  {
    dealTitle: 'PRICES SLASHED! No Fly Monte Carlo & Barcelona',
    cruiseCompany: 'Royal Caribbean',
    shipName: 'Independence of the Seas',
    date: '1 June 2019',
    nights: '14',
    itinerary: [
      'Southampton',
      'Gibraltar',
      'Monte Carlo(Cannes)',
      'Barcelona',
      'Valencia',
      'Malaga',
      'Lisbon(overnight)',
      'Vigo',
      'Southampton'
    ],
    price: '999',
    descriptionPoints: [
      'Balcony Cabins from£ 1699 pp',
      '$100 per cabin FREE Bonus Onboard Spend From www.CRUISE.co.uk',
    ],
    detailsLinkName: 'Click Here To View All Royal Caribbean 2019 Cruises',
    detailsLink: 'https://www.cruise.co.uk/all-2019-cruises/operatorId=17/',
    quoteLink: 'https://www.cruise.co.uk/quote-137375/?linkid=top10deals/'
  },
  {
    dealTitle: '5* East to West Coast USA with The Big Apple, Las Vegas & California Beach Stay',
    cruiseCompany: 'Cunard ',
    shipName: 'Queen Mary 2',
    date: '28 April 2019',
    nights: '14',
    itinerary: [
      'Southampton',
      'At Sea',
      'New York - Disembark - 2nt 4 * Hotel Stay',
      'Fly New York - Las Vegas - 2nt 4 * Hotel Stay',
      'Fly Las Vegas - Los Angeles - 2nt 4 * Hotel Stay',
      'Fly UK',
    ],
    price: '1999',
    descriptionPoints: [
      '2 Nights 4 * Stay In New York, Las Vegas & Long Beach, California',
      '7 Nights 5 * Transatlantic Cruising From Southampton',
      'All Direct Flights & Private Overseas Transfers Included ',
    ],
    detailsLinkName: 'View All Cunard Tailormade Cruises',
    detailsLink: 'https://www.cruise.co.uk/holidays/cunard-tailor-made-holidays/',
    quoteLink: 'https://www.cruise.co.uk/tailor-made-cruises/cunard-cruises/queen-mary-2/28-apr-19/18420/?linkid=top10deals/'
  },
  {
    dealTitle: 'Australia & New Zealand Odyssey With Opera House & Blue Mountains',
    cruiseCompany: 'Princess Cruises',
    shipName: 'Majestic Princess',
    date: '09 Nov 2019',
    nights: '19',
    itinerary: [
      'Fly UK to Sydney - 3nt Hotel stay',
      'Embark Majestic Princess Calling at',
      'Fiordland National Park',
      'Dunedin',
      'Akaroa',
      'Wellington',
      'Tauranga',
      'Auckland',
      'Bay of Islands',
      'Sydney',
      'Brisbane',
      'Airlie Beach',
      'Yorkey’ s Knob',
    ],
    price: '3699',
    descriptionPoints: [
      'FREE Sydney Opera House Tour & Blue Mountains Tour',
      '3 Night 4 * Central Stay In Sydney',
    ],
    detailsLinkName: 'View More Princess Tailor Made Cruises',
    detailsLink: 'https://www.cruise.co.uk/holidays/princess-tailor-made-holidays/',
    quoteLink: 'https://www.cruise.co.uk/tailor-made-cruises/princess-cruises/majestic-princess/09-Nov-19/17524/?linkid=top10deals'
  },
  {
    dealTitle: 'FREE Drinks & Wi-Fi! No Fly Paris & Spain',
    cruiseCompany: 'Royal Caribbean',
    shipName: 'Explorer of the Seas',
    date: '5 October 2019',
    nights: '6',
    itinerary: [
      'Southampton',
      'Vigo',
      'Gijon',
      'Paris (Le Havre)',
      'Southampton'
    ],
    price: '699',
    detailsLinkName: 'View More Royal Caribbean Cruises from Southampton',
    detailsLink: 'https://www.cruise.co.uk/holidays/royal-caribbean-southampton-sailings-2019/?linkid=top10deals',
    descriptionPoints: [
      'FREE Drinks worth $780 per Cabin - Book By 5 Nov 18',
      'Book with www.CRUISE.co.uk for £1pp Deposit & $40 per cabin Exclusive Onboard Spend'
    ],
    quoteLink: 'https://www.cruise.co.uk/checkAvailability.php?offerId=145285&DOLIVE=YES&linkid=top10deals/'
  },
  {
    dealTitle: 'New Price Drop! The Beaches Of Bali With The Best of Thailand & Vietnam',
    cruiseCompany: 'Princess Cruises',
    shipName: 'Sapphire Princess',
    date: '20 Feb 2020',
    nights: '16',
    itinerary: [
      'Fly UK to Singapore - 2 Nights Hotel Stay',
      'Ko Samui(Thailand)',
      'Bangkok(Laem Chabang)',
      'Ho Chi Minh City(Vietnam)',
      'Singapore',
      'Fly to Bali - 5 Nights Hotel Stay',
      'Fly UK'
    ],
    price: '1649',
    descriptionPoints: [
      '5 Nights 4 * B & B Hotel Stay in Bali',
      '2 Nights 5 * Central Hotel Stay in Singapore'
    ],
    detailsLinkName: 'Click Here For Detailed Itinarary',
    detailsLink: 'https://www.cruise.co.uk/tailor-made-cruises/princess-cruises/sapphire-princess/20-feb-20/18423/?linkid=top10deals',
    quoteLink: 'https://www.cruise.co.uk/quote-147120/?linkid=top10deals'
  },
  {
    dealTitle: 'BEST SELLER! No-Fly Canary Islands',
    cruiseCompany: 'Princess Cruises',
    shipName: 'Sapphire Princess',
    date: '16 Apr 2019',
    nights: '11',
    itinerary: [
      'Southampton',
      'Vigo',
      'Madeira',
      'Tenerife',
      'Las Palmas',
      'Lanzarote',
      'Southampton'
    ],
    price: '1149',
    descriptionPoints: [
      'Up to $400 Per Cabin Onboard Spend From Princess Cruises',
      'Book With www.CRUISE.co.uk For £1pp Deposit * & $60 FREE Bonus Onboard Spend Per Cabin'
    ],
    detailsLinkName: 'View All 2019 Princess Cruises from Southampton in One Place',
    detailsLink: 'https://www.cruise.co.uk/holidays/princess-2019-cruises-from-southampton/?linkid=top10deals/',
    quoteLink: 'https://www.cruise.co.uk/checkAvailability.php?offerId=133781&DOLIVE=YES/?linkid=top10deals/'
  },
  {
    dealTitle: 'BEST SELLER! 5* Baltics & Scandinavia with Overnight St. Petersburg',
    cruiseCompany: 'Cunard ',
    shipName: 'Queen Elizabeth',
    date: '11 August 2019',
    nights: '14',
    itinerary: [
      'Southampton',
      'Skagen(Denmark)',
      'Copenhagen',
      'St.Petersburg(Overnight)',
      'Helsinki',
      'Stockholm',
      'Warnemunde(Germany)',
      'Southampton'
    ],
    price: '1299',
    descriptionPoints: [
      'Spend 2 Days Onboard in Iconic St.Petersburg',
      'Book with www.CRUISE.co.uk for £1pp Deposit & $70 per cabin Exclusive Onboard Spend'
    ],
    detailsLinkName: 'View All Cunard 2019 Deals from Southampton',
    detailsLink: 'https://www.cruise.co.uk/holidays/cunard-2019-cruises-from-southampton/?linkid=top10deals',
    quoteLink: 'https://www.cruise.co.uk/checkAvailability.php?offerId=129547&DOLIVE=YES/&noint=1&linkid=top10deals/'
  },
  {
    dealTitle: 'NEW EXCLUSIVE! Sri Lanka, Dubai, Goa & Thailand with All Inclusive Drinks',
    cruiseCompany: 'Marella Cruises',
    shipName: 'Marella Discovery',
    date: '06 Dec 2018',
    nights: '14',
    itinerary: [
      'Dubai(Port Rashid - Overnight)',
      'India(Mumbai)',
      'Goa',
      'Mangalore',
      'Kerala(Cochin - Overnight)',
      'Sri Lanka(Colombo)',
      'Indonesia(Sabang)',
      'Thailand(Phuket)',
      'Malaysia(Langkawi)'
    ],
    price: '1129',
    descriptionPoints: [
      'FREE All Inclusive Drinks, Entertainment, Tips & Service Charges ',
      'Direct Return Flights from Gatwick - Birmingham Available *'
    ],
    detailsLinkName: 'View All 2019 Marella Cruises with Drinks',
    detailsLink: 'https://www.cruise.co.uk/holidays/marella-2019-cruises/',
    quoteLink: 'https://www.cruise.co.uk/checkAvailability.php?offerId=130921&DOLIVE=YES/'
  },
  {
    dealTitle: 'EXCLUSIVE - Capetown To Venice With Seychelles & Mauritius Overnights',
    cruiseCompany: 'MSC Cruises',
    shipName: 'Orchestra ',
    date: '10 April 2020',
    nights: '33',
    itinerary: [
      '3 Night Stay in Central Cape Town',
      'Port Elizabeth',
      'Durban',
      'Reunion',
      'Mauritius(Port Louis - Overnight)',
      'Seychelles(Port Victoria - Overnight)',
      'Jordan(Aqaba)',
      'Suez Canal Transit',
      'Katakolon(Greece)',
      'Split'
    ],
    price: '2399',
    descriptionPoints: [
      '2 Tours Included: Table Mountain With City Tour & Winelands Experience',
      '3 Nights 4 * Central Capetown Hotel Stay',
      '29 Nights Full Board Crusing Onboard MSC Orchestra'
    ],
    detailsLinkName: 'Click Here To View The Full Details',
    detailsLink: 'https://www.cruise.co.uk/tailor-made-cruises/msc-cruises/msc-orchestra/10-apr-20/18387/',
    quoteLink: 'https://www.cruise.co.uk/tailor-made-cruises/msc-cruises/msc-orchestra/10-apr-20/18387/'
  },
  {
    dealTitle: 'PRICES SLASHED! No Fly Monte Carlo & Barcelona',
    cruiseCompany: 'Royal Caribbean',
    shipName: 'Independence of the Seas',
    date: '1 June 2019',
    nights: '14',
    itinerary: [
      'Southampton',
      'Gibraltar',
      'Monte Carlo(Cannes)',
      'Barcelona',
      'Valencia',
      'Malaga',
      'Lisbon(overnight)',
      'Vigo',
      'Southampton'
    ],
    price: '999',
    descriptionPoints: [
      'Balcony Cabins from£ 1699 pp',
      '$100 per cabin FREE Bonus Onboard Spend From www.CRUISE.co.uk',
    ],
    detailsLinkName: 'Click Here To View All Royal Caribbean 2019 Cruises',
    detailsLink: 'https://www.cruise.co.uk/all-2019-cruises/operatorId=17/',
    quoteLink: 'https://www.cruise.co.uk/quote-137375/?linkid=top10deals/'
  },
  {
    dealTitle: '5* East to West Coast USA with The Big Apple, Las Vegas & California Beach Stay',
    cruiseCompany: 'Cunard ',
    shipName: 'Queen Mary 2',
    date: '28 April 2019',
    nights: '14',
    itinerary: [
      'Southampton',
      'At Sea',
      'New York - Disembark - 2nt 4 * Hotel Stay',
      'Fly New York - Las Vegas - 2nt 4 * Hotel Stay',
      'Fly Las Vegas - Los Angeles - 2nt 4 * Hotel Stay',
      'Fly UK',
    ],
    price: '1999',
    descriptionPoints: [
      '2 Nights 4 * Stay In New York, Las Vegas & Long Beach, California',
      '7 Nights 5 * Transatlantic Cruising From Southampton',
      'All Direct Flights & Private Overseas Transfers Included ',
    ],
    detailsLinkName: 'View All Cunard Tailormade Cruises',
    detailsLink: 'https://www.cruise.co.uk/holidays/cunard-tailor-made-holidays/',
    quoteLink: 'https://www.cruise.co.uk/tailor-made-cruises/cunard-cruises/queen-mary-2/28-apr-19/18420/?linkid=top10deals/'
  },
  {
    dealTitle: 'Australia & New Zealand Odyssey With Opera House & Blue Mountains',
    cruiseCompany: 'Princess Cruises',
    shipName: 'Majestic Princess',
    date: '09 Nov 2019',
    nights: '19',
    itinerary: [
      'Fly UK to Sydney - 3nt Hotel stay',
      'Embark Majestic Princess Calling at',
      'Fiordland National Park',
      'Dunedin',
      'Akaroa',
      'Wellington',
      'Tauranga',
      'Auckland',
      'Bay of Islands',
      'Sydney',
      'Brisbane',
      'Airlie Beach',
      'Yorkey’ s Knob',
    ],
    price: '3699',
    descriptionPoints: [
      'FREE Sydney Opera House Tour & Blue Mountains Tour',
      '3 Night 4 * Central Stay In Sydney',
    ],
    detailsLinkName: 'View More Princess Tailor Made Cruises',
    detailsLink: 'https://www.cruise.co.uk/holidays/princess-tailor-made-holidays/',
    quoteLink: 'https://www.cruise.co.uk/tailor-made-cruises/princess-cruises/majestic-princess/09-Nov-19/17524/?linkid=top10deals'
  }
]
export default api
