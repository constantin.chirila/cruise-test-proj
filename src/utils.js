import api from './data/api'


export const getList = (limit, page) => {
  let list = {
    pages: 1,
    deals: api
  }

  if(api.length > limit) {
    list.pages = ((api.length / limit) >> 0) + 1
    list.deals = api.slice((page - 1)*limit, limit * page)
  }

  return list
}
